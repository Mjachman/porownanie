﻿using Ninject;

namespace DodatkowaPraca
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var kernel = new StandardKernel(new ProgramModule());
            var flow = kernel.Get<GameFlow>();

            flow.Run();
        }
    }
}
