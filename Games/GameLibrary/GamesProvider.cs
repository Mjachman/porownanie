﻿using System.Linq;
using Infrastructure;

namespace GameLibrary
{
    public class GamesProvider : IGamesProvider
    {
        private readonly IGame[] _games;
        private IGame _chosenGame;

        public GamesProvider(IGame[] games)
        {
            this._games = games;
        }

        public string[] GetGameNames()
        {
            return _games.Select(g => g.Name).ToArray();
        }

        public void ChooseGame(int n)
        {
            _chosenGame = _games[n];
        }

        public void ChooseGame(string name)
        {
            _chosenGame = _games.First(g => g.Name == name);
        }

        public bool Guess(string bet, out string result)
        {
            return _chosenGame.Guess(bet, out result);
        }

        public int Result
        {
            get { return _chosenGame.Result; }
        }
    }
}
